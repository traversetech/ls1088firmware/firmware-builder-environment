FROM debian:bookworm
RUN apt-get update && \
	apt-get -y install build-essential curl libssl-dev tcl git python3 bison flex bc \
		device-tree-compiler parted dosfstools mtools u-boot-tools xxd file && \
	rm -rf /var/lib/apt/lists/*
RUN cd /tmp && curl -L https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz -J -L -o /tmp/toolchain.tar.xz && \
	mkdir -p /opt && cd /opt && tar -Jxf /tmp/toolchain.tar.xz && \
	rm /tmp/toolchain.tar.xz
ENV CROSS_COMPILE /opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
RUN useradd -m build
USER build
